# -*- coding: utf-8 -*-
"""
Created on Mon Aug  1 13:05:58 2020

@author: shivaraja.ts
"""

import logging
import threading
import time
import streamlit as st
from bokeh.plotting import figure
# Import classes from the Weather package
from Weather import Weather
st.header('Weather Report')

# Create an object of Weather class
weatherobj = Weather()


#alsajs
#Accessing the class variables using objects
base_url=weatherobj.base_url
api_key=weatherobj.api_key

city_name=st.text_input('Enter the City Name : ')
submit=st.button('Submit')

def city_found(name):
    time.sleep(2)
    logging.info("%s", name)


if __name__ == "__main__":
    if submit:
        #Calling a method of the Weather class
        data,response_val=weatherobj.getResponse(base_url,api_key,city_name)
        t1 = threading.Thread(target=city_found, args=(response_val,)) 
        t1.start()
        t1.join()
        if response_val=='City Found':        
            data=data.set_index('Date')
            st.dataframe(data)
            st.write('\n\n\n')
            st.area_chart(data['Temperature'])
            
        else:
            st.write(response_val)
    