# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 20:47:42 2020

@author: shivaraja.ts
"""
import requests
import pandas as pd
from datetime import timedelta, date


class Weather:
    def __init__(self):
        ''' Constructor for this class. '''
        # Create some member weather
        self.api_key="c0c4a4b4047b97ebc5948ac9c48c0559"
        # base_url variable to store url 
        self.base_url = "http://api.openweathermap.org/data/2.5/forecast/daily?q=" 
 
    def getResponse(self,base_url,api_key,city_name):
        #Created the complete url using this extract the information from openweather.org
        complete_url = base_url+city_name+"&units=metric&cnt=6&lang=en&appid="+api_key 
        # return response object 
        response = requests.get(complete_url) 
        # json method of response object 
        # convert json format data into 
        # python format data 
        x = response.json() 
        #Exception Handling to check the city user input added to the API is found or not
        try:
            if x['cod']:
                #defining the column names to show in the frontend and store in file
                cols=['Date','City Name','Temperature','Humidity','Pressure','Latitude','Longitude','Population','Description']
                dataframe=pd.DataFrame(columns=cols)
                #Extracting the info from the response json
                dataframe['City Name']=[x['city']['name']]*6
                dataframe['Latitude']=x['city']['coord']['lat']
                dataframe['Longitude']=x['city']['coord']['lon']
                dataframe['Population']=x['city']['population']
                for i in range(6):
                    dataframe['Temperature'][i]=x['list'][i]['temp']['max']
                    dataframe['Humidity'][i]=x['list'][i]['humidity']
                    dataframe['Description'][i]=x['list'][i]['weather'][0]['description']
                    dataframe['Pressure'][i]=x['list'][i]['pressure']
                date_list=[]
                for k in range(6):
                    date_list.append(date.today() + timedelta(days=k))
                dataframe['Date']=date_list
                #Output the result into csv file to access
                dataframe.to_csv('output.csv',index=False)
                response_val='City Found'
        except:
            response_val='City Not Found'
        return dataframe,response_val